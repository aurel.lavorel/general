### For New Events 

Event Details: 
* http:
* Salesforce campaign:
* [Campaign Influence Report]()
* Location: 
* Speakers: 
* Attendees:
* Staffing Needs:
* [Sponsorship]():
* Swag: 
* Lodging: 
* Expo Center: 
* Audience Demographics/ Breakdown:
* Expected returns (ROI):
  * # of scanned leads
  * # SCLAU net new
  * net new pipeline
  * influenced opportunities and pipeline
* Other Benefits of attending/ sponsorship:
* Total costs (sponsorship + travel + collateral + etc.):

Capaign Checklist- Marketing Operations
* [ ] Campaign start and end date
* [ ] Event budget 
* [ ] goals for # of leads 
* [ ] goals for # SCLAU
* [ ] # meetings at event
* [ ] registration goals/ attendance goals
* [ ] other
* [ ] SFDC Campaign created + marketo integration

Event Checklist:
* [ ] Contract Signed
* [ ] Artwork sent
* [ ] Booth Art Work sent- custom artwork included in package 
* [ ] Speaker Brief Sent
* [ ] Slack channel created and attendees invited
* [ ] Tickets allocated   
* [ ] Attendee directory
* [ ] Press list sent 
* [ ] Badge scanners
* [ ] Booth duty scheduling
* [ ] Social media copy written and scheduled
* [ ] Select hotel
* [ ] Everyone to book into same hotel
* [ ] Flights/ transport booked
* [ ] Booth slideshow/ demo (shared with staff)
* [ ] Final prep meeting scheduled
* [ ] Event Post Mortem Shceduled 
* [ ] Leads shared with team
* [ ] After event follow up set up in Marketo
* [ ] After event survey sent 

Outreach Checklist:
* [ ] Calendar links provided to XDR team for outreach- Create Meeting Schedule in calendly
* [ ] SDRs set up in person meetings at event
* [ ] Update email signatures- Luke to provide design
* [ ] Download Event App
* [ ] Provide attendee list when available
* [ ] If not available pass list of attendees and speakers to Leadware
* [ ] Mailing from sfdc / marketo- copy procided
* [ ] Join LinkedIn group
* [ ] After event follow up set up in Marketo

## Expo Schedule:


## Other:

@emilykyle