## Customer Specific Webcast Request   

Please name your issue in the following format:  
**MM/DD {Company Name} (Host Name) Webcast**  
Example: 12/01 GitLab (JJ) Webcast   

(Please fill-in the following details as completely & concisely as possible)  
### Event Details   
**Customer Name**:   
**Date & Time of Event** (no less than 30 days from request date; include time zone):  
**AE/Sales Team Member**:  
**SDR** (if one is assigned to account or assisting with outreach):  
**SA Assigned to Webcast** (please arrange this before making this webcast request):   

### Landing Page Details   

**Desired Live Date** (no less than 48 BUSINESS hours from request):  
**Headline**:  
**Secondary Headline**:  
**Copy Paragraph**:  
**USP Bullet Points** (why they should attend the webcast & high-points of what they will learn):  
   * (Bullet 1)  
   * (Bullet 2)  
   * (Bullet 3)  
   * (Bullet 4)     


### Webcast Details   
If any of the following questions are answered with a "Yes" please provide prior to webcast date, does not need to be provided at the time of this request.  
By default, our webcast provider is Zoom, if you need to use WebEx, please indicate that in this issue. 

**Do you want a post event survey?** Yes / No    
**Will there be downloadable content included in your presentation?** Yes / No


### Sales Team Member Responsibilities  
* [ ] Coordinate webcast date with SA team  
* [ ] Write email copy & landing page copy - Content team can review, if needed  
* [ ] Send Invitation/queue Outreach sequence  
* [ ] Add anyone invited to Webcast to SFDC Campaign with the Member Status of `Invited`  
* [ ] Work with SA team to develop webcast slide deck   


### Marketing OPS Responsibilities  
* [ ] Unbounce Landing Page  
* [ ] SFDC Campaign  
* [ ] SFDC Reports - set to trigger to associated sales team member on 15th & 20th day preceding event  
* [ ] Marketo Program  
     * [ ] Registration Confirmation Email  
     * [ ] Post-event Follow-up  
* [ ] Webcast Link   

/label ~"Marketing Ops"  ~Events   
/cc @EmilyKyle
/assign @jjcordz 