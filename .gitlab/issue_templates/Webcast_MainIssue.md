## Title 

**Event date:** _Insert Event Date and Time Here_ (example: 3/23 @ 9:15am PDT - 12:15pm EDT - 4:15pm UTC)

**Speaker:** _Insert Speaker Here_ 
**PM:** _Insert Project Manager Here_
**Producer:** _Insert Producer Here_

**Insert link to deck here**

**Notes**
_Insert notes here_

**Research**

_Insert links to research here_

### Required steps to launch webcast:

- [ ] Date/time for webcast finalized
- [ ] Landing page web copy
- [ ] Registration confirmation email copy

### Proposed timeline:

- [ ]  Write copy for landing page _link to landing page issue here_ (_date for issue to be done_)

- [ ]  Create issue for design assets _link to design assets issue_ (_date for issue to be done_)

- [ ]  Write copy for emails & open issue _link to email copy issue_ (_date for issue to be done_)

- [ ]  Write abstract and outline (draft _link to abstract and outline_) (_date for issue to be done_)

- [ ]  Schedule promo tweets for 1 week, 3 days, 1 day and day of

- [ ]  Create deck outline (_date for issue to be done_)

- [ ]  Write and add CTA to blog post (_date for issue to be done_)

- [ ]  Presenters to fill in deck (_date for issue to be done_)

- [ ]  Share webcast deck for feedback (_date for issue to be done_)

- [ ]  Host webcast dry run (_date for issue to be done_)

- [ ]  Include CTA in newsletter (_date for issue to be done_)

- [ ]  Host webcast (_date for issue to be done_)

- [ ]  Upload recording to YouTube

- [ ]  Update and send follow-up emails (_day after webcast_)