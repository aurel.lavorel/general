### Terminus- banner ad targeting people who work at specific companies we designate
Read before submitting: ads take up to 3 days for temrinus to review. Design and copy creation takes at least a week. Ads targeting will stop once an opportunity created in company name. 

For Sales:
* Industry (if we are trageting banking for example):
* Sector (strategic, large, mid market...):
* Targeted company/ companies:
* Titles targeted (default is IT management):
* Goal:
* Url ads will link to:
* Copy if you have it:

For Marketing Operations:
* Campaign Start Date:
* End Date:
* Associated Campaigns:
* Spend:
* desired ad style/ size (see below)

Design:
* [ ] Artwork created
* [ ] Copy created
* 

Ad sizes:
* mobile: 320 x 50
* desktop: 
  *  300x600
  *  160x600
  *  300x250
  *  728x90
