_Insert Webcast Title Here_

**Event date:** _Insert Event Date and Time Here_ (example: 3/23 @ 9:15am PDT - 12:15pm EDT - 4:15pm UTC)

**Speaker:** _Insert Speaker Here_    
**PM:** _Insert Project Manager Here_   
**Producer:** _Insert Producer Here_   

**[DECK](https://docs.google.com/a/gitlab.com/presentation/d/1B7EvZIKa7Cl1QwscufSFgwlZJbS2ySd0btwVSW5POMI/edit?usp=sharing)**

- [ ] Landing page updated and synced with Marketo  (@jjcordz)

- [ ] Confirmation email updated  (@jjcordz)

- [ ] Presenter, PM and Q&A support team members added as panelists in Zoom  (@jjcordz)

- [ ] Calendar event created with relevant team members invited (PM for event)

- [ ] Dry run scheduled  (PM for event)

- [ ] Poll questions written and sent to Marketing OPS before dry run

- [ ] Slide deck updated with presenter info and any new features

- [ ] Host webcast - Will be **automatically recorded** and saved onto host's local computer

- [ ] Upload recording to YouTube, set visibility to public and make previous enterprise demo video unlisted

- [ ] Add link to recording next to presenter's name in [SA scheduling issue](https://gitlab.com/gitlab-com/customer-success/sa-service-desk/issues/268)

- [ ] Update emails (PM for event)   
- 
- [ ] Send follow-up emails (@jjcordz)