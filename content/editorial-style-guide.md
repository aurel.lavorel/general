# Editorial style guide

The following guide outlines the set of standards used for all written company 
communications to ensure consistency in voice, style, and personality across all 
of GitLab's public communications. 

## Table of contents

- [About](#about)
  - [GitLab the community](#community)
  - [GitLab the company](#company)
  - [GitLab the product](#product)
- [Tone of voice](#voice)
- [General style guidelines](#guidelines)
- [Abbreviations](#abbreviations)
- [Acronyms](#acronyms)
- [Ampersands](#ampersands)

## About

### GitLab the community

GitLab is an [open source project](https://gitlab.com/gitlab-org/gitlab-ce/) 
with a large community of contributors. Over 1,800 people worldwide have 
contributed to GitLab's source code. 

### GitLab the company

GitLab Inc. is a company based on the GitLab open source project. GitLab Inc. is
an active participant in our community (see our [stewardship of GitLab CE](https://about.gitlab.com/stewardship)
for more information), as well as offering GitLab, a product (see below).

### GitLab the product

GitLab is an integrated product that unifies issues, code review, CI and CD into a single UI.
GitLab Inc. offers [self-hosted products](https://about.gitlab.com/products) and
[SaaS plans for GitLab.com](https://about.gitlab.com/gitlab-com).

## Tone of voice

The tone of voice we use when speaking as GitLab should always be informed by 
our [Content Strategy](https://gitlab.com/gitlab-com/marketing/blob/master/content/content-strategy.md#strategy).
Most importantly, we see our audience as co-conspirators, working together to 
define and create the next generation of software development practices. The below
table should help to clarify further:


<table class="tg">
  <tr>
    <th class="tg-yw4l">We are:</th>
    <th class="tg-yw4l">We aren't:</th>
  </tr>
  <tr>
    <td class="tg-yw4l">Equals in our community</td>
    <td class="tg-yw4l">Superior</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Knowledgeable</td>
    <td class="tg-yw4l">Know-it-alls</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Empathetic</td>
    <td class="tg-yw4l">Patronizing</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Straightforward</td>
    <td class="tg-yw4l">Verbose</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Irreverent</td>
    <td class="tg-yw4l">Disrespectful</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Playful</td>
    <td class="tg-yw4l">Jokey</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Helpful</td>
    <td class="tg-yw4l">Dictatorial</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Transparent</td>
    <td class="tg-yw4l">Opaque</td>
  </tr>
  <tr>
    <td class="tg-yw4l"></td>
    <td class="tg-yw4l"></td>
  </tr>
</table>

We explain things in the simplest way possible, using plain, accessible language. 

We keep a sense of humor about things, but don't make light of serious issues or
problems our users or customers face. 

We use colloquialisms and slang, but sparingly (don't look like you're trying too hard!).

We use [inclusive, gender-neutral language](https://litreactor.com/columns/5-easy-ways-to-make-your-writing-gender-neutral). 

## General style guidelines

We use American English by default and follow the AP Style guide unless otherwise 
noted in this style guide. We do not use title case. 

## Abbreviations



## Acronyms

When using [acryonyms or initialisms](https://www.dailywritingtips.com/acronym-vs-initialism/),
ensure that your first mention uses the full term and includes the shortened 
version in parentheses directly afterwards. From then on you 
may use the acronym or initialism instead. 

> Example: A Contributor License Agreement (CLA) is the industry standard for open
source contributions to other projects.

Below are some common acroynms and initialisms we use which don't need to be 
defined first (but use sparingly, see [Tone of voice](#tone-of-voice) above):

- AFAIK - as far as I know
- ICYMI - in case you missed it (for social only)
- IIRC - if I recall correctly
- IRL - in real life
- TL;DR - too long; didn't read

## Ampersands

Use ampersands only where they are part of a company's name or the title of a 
publication or similar. Example: Barnes & Noble

Do not use as a substitute for "and."

## Capitalization
Use [sentence case](https://www.thoughtco.com/sentence-case-titles-1691944) for 
all titles and headings.

All GitLab feature names must be capitalized. Examples: GitLab Issue Boards, 
GitLab Pages.

See [below](#word-list) for styling of specific words.

## Contractions

We favor contractions ("can't," "didn't," "it's," "we're") to sound more human and 
less formal. 

## Formatting 
## Lists
## Numbers

Spell out one to nine. Use numerals for 10 upwards. Try to avoid beginning a 
sentence with a number, but if unavoidable, spell it out.

Numbers with four or more digits should include a comma. Examples: 1,800; 100,000

## Punctuation

Only one space after a period is necessary.

Include one space after ellipses (... )

See [below](#word-list) for when to hyphenate specific words.

We use en dashes (–) rather than em dashes (—). Include a space before and after the dash.

## Quotes

Use double quotation marks for direct quotes, and single quotation marks for a quote
within a quote. Single quotation marks may also be used for specialist terms or sayings.

Include punctuation in quotation marks. 

> Example: What do you think of the claim, "software is eating the world?"

## Word choice

When in doubt, use the "future" styling of a word. So, "internet" is not capitalized,
"startup" is not hyphenated, etc.

## Word list

How to spell and style commonly used words.

- Agile
   - A is capitalized when referring to [Agile methodology](https://en.wikipedia.org/wiki/Agile_software_development)
- cloud native
    - not capitalized, and no hyphen, regardless of how it is used
- continuous delivery, deployment, integration
    - not capitalized
- developer
    - may only be shortened to "dev" on social
- DevOps 
    - D and O always capitalized
- Git
    - always capitalized
- GitHub
- GitLab
    - G and L are always capitalized, even in GitLab.com
- internet
    - not capitalized
- Kubernetes
    - always capitalized, never abbreviated to K8s (except on social)
- open source
    - no hyphen, regardless of how it is used
- remote-only/remote only
    - hyphenated only when appearing before a noun ("GitLab is a remote-only organization"/"GitLab is remote only")
- set up/setup
    - two words for the verb, one for the noun ("How to set up a Kubernetes cluster"/"Let's walk through the Kubernetes cluster setup process")
- startup
    - no hyphen

## Appendix A: When to use en dashes and semicolons 


## Appendix B: UK vs. American English

Use American spelling in your communications. Please consult [this list of 
spelling differences](https://en.oxforddictionaries.com/spelling/british-and-spelling).


